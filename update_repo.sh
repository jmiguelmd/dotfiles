#!/bin/bash 

# Update the repository with the current system configuration

pacman -Qqen > pacman/pkglist
pacman -Qqem > pacman/aur_packages

cp ~/.config/i3/config i3/config
cp ~/.xinitrc xinit/.xinitrc

cp ~/.vimrc vim/
sudo cp -r ~/.vim vim/

cp ~/.config/zathura/zathurarc zathura/
