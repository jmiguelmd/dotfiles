set number relativenumber
set idearefactormode=keep
set ideajoin
set surround
set easymotion

let mapleader = " "

nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>

nnoremap <C-j> :m +1<CR>
nnoremap <C-k> :m -2<CR>
inoremap <C-j> <Esc>:m +1<CR>gi
inoremap <C-k> <Esc>:m -2<CR>gi

" system clipboard
set clipboard+=unnamed
vmap <leader>y "+y
vmap <leader>d "+d
nmap <leader>y "+yy
nmap <leader>p "+p
nmap <leader>P "+P
vmap <leader>p "+p
vmap <leader>P "+P

nnoremap <leader><leader> :action Switcher<CR>

nmap <leader>h <action>(PreviousTab)
nmap <leader>l <action>(NextTab)

set ignorecase
set smartcase

nmap <leader>rr <action>(Run)
nmap <leader>rc <action>(ChooseRunConfiguration)
nmap <leader>ut <action>(RunConfigurationAsAction_Run_moon unit_default_target)
nmap <leader>it <action>(RunConfigurationAsAction_Run_moon integration_default_target)
nmap <leader>gut <action>(RunConfigurationAsAction_Run_grok unit_default_target)
nmap <leader>git <action>(RunConfigurationAsAction_Run_grok integration_default_target)
nmap <leader>gbt <action>(RunConfigurationAsAction_Run_grok billing_default_target)
nmap <leader>rn <action>(RenameElement)
nmap <leader>il <action>(Inline)
nmap <leader>oi <action>(OptimizeImports)
nmap <leader>em <action>(ExtractMethod)
nmap <leader>iv <action>(IntroduceVariable)
nmap <leader>fu <action>(FindUsages)
nmap <leader>sd <action>(SafeDelete)
nmap <leader>pu <action>(MembersPullUp)
nmap <leader>if <action>(IntroduceField)
nmap <leader>ic <action>(IntroduceConstant)
nmap <leader>co <action>(CloseAllEditorsButActive)
nmap <leader>ca <action>(CloseAllEditors)
nmap <leader>pv <action>(SelectInProjectView)
nmap <leader>cw <action>(CloseActiveTab)
nmap <leader>ip <action>(GotoImplementation)
nmap <leader>cs <action>(ChangeSignature)
nmap <leader>sm <action>(GotoSuperMethod)
nmap <leader>bp <action>(ToggleLineBreakpoint)
nmap <leader>se <action>(ShowErrorDescription)
nmap <leader>si <action>(StepInto)
nmap <leader>so <action>(StepOut)
nmap <leader>sv <action>(StepOver)
nmap <leader>nb <action>(RunToCursor)

" Show actions like "Import class"
nnoremap <leader>a <action>(ShowIntentionActions)
nnoremap <leader>w <action>(ReformatCode)
nnoremap <leader>bw <action>(Back)
nnoremap <leader>fw <action>(Forward)

" Go to tabs
nnoremap <leader>1 <action>(GoToTab1)
nnoremap <leader>2 <action>(GoToTab2)
nnoremap <leader>3 <action>(GoToTab3)
nnoremap <leader>4 <action>(GoToTab4)
nnoremap <leader>5 <action>(GoToTab5)
nnoremap <leader>6 <action>(GoToTab6)
nnoremap <leader>7 <action>(GoToTab7)
nnoremap <leader>8 <action>(GoToTab8)
nnoremap <leader>9 <action>(GoToTab9)

set NERDTree
let g:NERDTreeMapActivateNode='l'
let g:NERDTreeMapJumpParent='h'