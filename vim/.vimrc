set nocompatible

let mapleader = ","

set paste
set relativenumber
set showcmd
set ruler

" Search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**

set splitright

syntax on

nnoremap <leader>dt :!python manage.py test arranger.tests<CR>

" {{{ perl related commands
nnoremap <leader>w :w<CR>
nnoremap <leader>p :!perl %<CR>
nnoremap <leader>t :!prove %<CR>
nnoremap <leader>f :!perltidy -pro=/home/nonius/.perltidyrc -b %<CR>
" }}}

" {{{ switch to tab page
nnoremap <leader>1 1gt
nnoremap <leader>2 2gt
nnoremap <leader>3 3gt
nnoremap <leader>4 4gt
nnoremap <leader>5 5gt
nnoremap <leader>6 6gt
nnoremap <leader>7 7gt
nnoremap <leader>8 8gt
nnoremap <leader>9 9gt
" }}}

"" vim:fdm=marker
